function values(object){
    allValues = []
    for(let key in object){
        if (object.hasOwnProperty(key)) {
            allValues.push(object[key])
        }
   }
   return allValues
}

module.exports = values
