function keys(object){
    allKeys = []
    for(let key in object){
        if(object.hasOwnProperty(key) ){
            allKeys.push(key)
        }
   }
   return allKeys
}

module.exports = keys
