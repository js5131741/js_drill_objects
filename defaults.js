function defaults(object, defaultProps){
    for (let key in defaultProps){
        if (defaultProps.hasOwnProperty(key) && object[key] === undefined){
            object[key] = defaultProps[key]
        }
    }
}

module.exports = defaults