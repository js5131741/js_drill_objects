function invert(object){
    invertedObject = {}
    for(let key in object){
        if (object.hasOwnProperty(key)){
            invertedObject[object[key]] = key
        }
    }
    return invertedObject
}

module.exports = invert