function pairs(object){
    const result = []
    for(let key in object){
        if (object.hasOwnProperty(key)){
            result.push([key, object[key]])
        }
    }
    return result
} 


module.exports = pairs


